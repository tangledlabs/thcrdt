from thcrdt.thcrdt import CRDT

crdt = CRDT()

# Create document with initial state
doc1s: dict = crdt.from_({'cards': [{}]}).unwrap()


# CLIENT A
# changes on client A
def doc_ca(doc: dict):
    doc['cards'][0]['x'] = 5
    doc['cards'][0]['y'] = 10
    doc['cards'][0]['z'] = 20


# In order to update doc1s you should use crdt.change().
doc_a1: dict = crdt.change(crdt.clone(doc1s).unwrap(), doc_ca).unwrap()

# Get changes made on client A. These changes are encoded as byte arrays (Uint8Array)
doc_a1_changes: list = crdt.get_changes(doc1s, doc_a1).unwrap()


# CLIENT B
# Apply changes on client B
doc_b1, patch = crdt.apply_changes(doc1s, doc_a1_changes).unwrap()
