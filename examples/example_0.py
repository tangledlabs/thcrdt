from thcrdt.thcrdt import CRDT

crdt = CRDT()

# Create document with initial state
doc_s0: dict = crdt.from_({'cards': [{'x': 0}]}).unwrap()


# CLIENT A
# changes on client A
def doc_ca(doc: dict):
    doc['cards'][0]['x'] = 5
    doc['cards'][0]['y'] = 10
    doc['cards'][0]['z'] = 20


# The doc_s0 object is treated as immutable, you must never change it directly,
# create doc_s0 clone using crdt.clone().
# In order to update doc_a0 you should use crdt.change() instead.
doc_a1: dict = crdt.change(crdt.clone(doc_s0).unwrap(), doc_ca).unwrap()


# CLIENT B
# Create initial document on client B
doc_b0: dict = crdt.from_({'cards': [{}]}).unwrap()
doc_b1: dict = crdt.merge(doc_b0, doc_a1).unwrap()


# changes on client B
def doc_cb(doc: dict):
    doc['cards'][0]['x'] = -5
    doc['cards'][0]['y'] = -10
    doc['cards'][0]['z'] = -20


doc_b2: dict = crdt.change(crdt.clone(doc_b1).unwrap(), doc_cb).unwrap()

# Merge the changes from client B back into client A. You can also do the merge the other way round,
# and you'll get the same result.
final_doc: dict = crdt.merge(doc_b2, doc_a1).unwrap()
