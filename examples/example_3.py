from thcrdt.thcrdt import CRDT, Counter

crdt = CRDT()

doc1: dict = {'cards': [
    {
        'a': Counter(10),
        'b': Counter(20),
    }
]}

# Create CRDT Counter
doc1s: dict = crdt.from_(doc1).unwrap()


# CLIENT A
# changes on client A
def doc1a(doc: dict):
    doc['cards'][0]['a'].increment(2)
    doc['cards'][0]['b'].decrement(2)


# Use crdt.change() to update doc1s.
doc1a: dict = crdt.change(crdt.clone(doc1s).unwrap(), doc1a).unwrap()

# Get changes made on client A.
doc1a_changes: list = crdt.get_changes(doc1s, doc1a).unwrap()

# Update initial counter with changes
doc1s, patch = crdt.apply_changes(doc1s, doc1a_changes).unwrap()

